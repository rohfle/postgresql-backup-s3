#! /bin/sh

# Usage:
# restore.sh -f latest
# restore.sh -f <name of file>

# Args:
#  -f  Force database drop before restore

# Other notes:
#  - Uses "-Fc" format for pg_restore


set -e
set -o pipefail

>&2 echo "-----"

while getopts ":fh" opt; do
  case ${opt} in
    h )
      echo "Usage:"
      echo "    restore.sh -hf latest|<filename>"
      echo "Options:"
      echo "    -h       Display this help message."
      echo "    -f       Force drop of database before restore."
      exit 0
      ;;
    f )
      FORCE_DROPDB=y
      ;;
    \? )
      echo "Invalid Option: -$OPTARG" 1>&2
      exit 1
      ;;
  esac
done
shift $((OPTIND -1))
echo $1


if [ ! -z "${S3_ACCESS_KEY_ID_FILE}" ]; then
  S3_ACCESS_KEY_ID="$(cat "${S3_ACCESS_KEY_ID_FILE}")"
fi

if [ "${S3_ACCESS_KEY_ID}" = "**None**" ]; then
  echo "You need to set the S3_ACCESS_KEY_ID or S3_ACCESS_KEY_ID_FILE environment variable."
  exit 1
fi

if [ ! -z "${S3_SECRET_ACCESS_KEY_FILE}" ]; then
  S3_SECRET_ACCESS_KEY="$(cat "${S3_SECRET_ACCESS_KEY_FILE}")"
fi

if [ "${S3_SECRET_ACCESS_KEY}" = "**None**" ]; then
  echo "You need to set the S3_SECRET_ACCESS_KEY or S3_ACCESS_KEY_ID_FILE environment variable."
  exit 1
fi

if [ "${S3_BUCKET}" = "**None**" ]; then
  echo "You need to set the S3_BUCKET environment variable."
  exit 1
fi

if [ "${POSTGRES_DATABASE}" = "**None**" ]; then
  echo "You need to set the POSTGRES_DATABASE environment variable."
  exit 1
fi

if [ "${POSTGRES_HOST}" = "**None**" ]; then
  if [ -n "${POSTGRES_PORT_5432_TCP_ADDR}" ]; then
    POSTGRES_HOST=$POSTGRES_PORT_5432_TCP_ADDR
    POSTGRES_PORT=$POSTGRES_PORT_5432_TCP_PORT
  else
    echo "You need to set the POSTGRES_HOST environment variable."
    exit 1
  fi
fi

if [ "${POSTGRES_USER}" = "**None**" ]; then
  echo "You need to set the POSTGRES_USER environment variable."
  exit 1
fi

if [ ! -z "${POSTGRES_PASSWORD_FILE}" ]; then
  POSTGRES_PASSWORD="$(cat "${POSTGRES_PASSWORD_FILE}")"
fi

if [ "${POSTGRES_PASSWORD}" = "**None**" ]; then
  echo "You need to set the POSTGRES_PASSWORD environment variable or link to a container named POSTGRES."
  exit 1
fi

if [ "${S3_ENDPOINT}" == "**None**" ]; then
  AWS_ARGS=""
else
  AWS_ARGS="--endpoint-url ${S3_ENDPOINT}"
fi

# env vars needed for aws tools
export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION=$S3_REGION

export PGPASSWORD=$POSTGRES_PASSWORD
POSTGRES_HOST_OPTS="-h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USER $POSTGRES_EXTRA_OPTS"

echo "Restoring backup of ${POSTGRES_DATABASE} database from ${POSTGRES_HOST}..."

DEST_FILE=restore.custom.gz

if [ "$1" == "latest" ]; then
  SRC_FILE="$(aws $AWS_ARGS s3 ls s3://$S3_BUCKET/$S3_PREFIX/ | grep " PRE " -v | sort | awk {'print $4'} | grep "^${POSTGRES_DATABASE}_" | tail -n 1 || true)"
  if [ -z "$SRC_FILE" ]; then
    echo "No backups found for database ${POSTGRES_DATABASE}. Exiting..."
    exit 1
  fi
else
  SRC_FILE=$1
fi

aws $AWS_ARGS s3 cp "s3://$S3_BUCKET/$S3_PREFIX/$SRC_FILE" $DEST_FILE || exit 2

if [ "${ENCRYPTION_PASSWORD}" != "**None**" ]; then
  >&2 echo "Decrypting ${SRC_FILE}"
  mv "$DEST_FILE" "${DEST_FILE}.enc"
  openssl aes-256-cbc -d -in ${DEST_FILE}.enc -out "$DEST_FILE" -k $ENCRYPTION_PASSWORD
  if [ $? != 0 ]; then
    >&2 echo "Error decrypting ${SRC_FILE}.enc"
    exit 3
  fi
  rm ${DEST_FILE}.enc
fi

if psql $POSTGRES_HOST_OPTS -lqt | cut -d \| -f 1 | grep -qw "$POSTGRES_DATABASE"; then
  if [ ! -z "$FORCE_DROPDB" ]; then
    >&2 echo "$POSTGRES_DATABASE exists. Dropping database..."
  else
    read -p "Existing database $POSTGRES_DATABASE will be replaced. Do you want to continue? [y/N] " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) echo >&2 "Restore aborted." && exit;;
        * ) echo >&2 "Restore aborted." && exit;;
    esac
  fi
  dropdb $POSTGRES_HOST_OPTS ${POSTGRES_DATABASE}
fi

createdb $POSTGRES_HOST_OPTS -O ${POSTGRES_USER} ${POSTGRES_DATABASE}

gunzip -c "$DEST_FILE" | pg_restore -d ${POSTGRES_DATABASE} -Fc $POSTGRES_HOST_OPTS

echo "Postgres restore finished successfully."

>&2 echo "-----"
